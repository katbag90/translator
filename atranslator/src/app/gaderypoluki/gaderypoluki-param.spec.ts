import { GaDeRyPoLuKi } from './gaderypoluki';
// definicja tablicy obiektów, które stanowią wejście do jednego testu
// każdy obiekt zawiera wiadomość do przetłumaczenia i oczekiwaną wartość
const runs = [
  { msg: 'ala', expected: 'gug' },
  { msg: 'gala', expected: 'agug' },
  { msg: 'kot', expected: 'ipt' },
  { msg: 'wonsz', expected: 'wpnsz' },
];

describe('check different translations', () => {
  let gaderypoluki: GaDeRyPoLuKi;

  beforeEach(() => {
    gaderypoluki = new GaDeRyPoLuKi();
  });

  // zwykła iteracja po tablicy i tworzenie kolejnych testów
  runs.forEach(run => {
    it(`should translate ${run.msg} to ${run.expected}`, () => {
      const result = gaderypoluki.translate(run.msg);
      expect(result).toEqual(run.expected);
    });
  });


});
