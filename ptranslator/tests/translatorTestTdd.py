import pytest
from gaderypoluki.gaderypoluki import GaDeRyPoLuKi


# tdd wariant 2
@pytest.fixture
def translator():
    translator = GaDeRyPoLuKi()
    return translator


def test_should_check_case(translator):
    # given
    msg = "LOk"
    #when
    result = translator.translate(msg)
    #then
    assert result == "UPi"