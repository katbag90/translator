package pl.poznan.put.spio.controller.model;

import lombok.Builder;

@Builder
public class ErrorResponse {
    private final String message;
}
